<?
class cronjob__cronjob extends cronjob__cronjob__parent
{
	function load($d = null)
	{
		parent::{__function__}();

		$this->C->cronjob()->get_cronjob();
		$now = date('YmdHi');
		foreach($this->D['SETTING']['CRONJOB']['D'] as $kCRON => $vCRON )
		{
			if($vCRON['ACTIVE'] && $now >= $vCRON['SDATETIME'])
			{
				#1...ausf�hren
				file_get_contents($vCRON['URL']);
				#2. Speichern
				$this->D['SETTING']['CRONJOB']['D'][$kCRON]['SDATETIME'] = $now+$vCRON['INTERVAL'];
				#ToDo: Log
			}
		}
		
		$this->C->cronjob()->set_cronjob();
	}
}